import Data.List
main = do
    lineGet

lineGet = do
    line <- getLine
    doLine line
    lineGet

doLine x = do
    if isInfixOf "ERROR" x
        then do
            putStrLn(x)
        else return()
