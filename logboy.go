package main

import (
	"time"

	"gitlab.com/okotek/okolog"
)

func main() {

	go okolog.StringLoggerListener()

	for {
		time.Sleep(10000 * time.Hour)
	}
}
