module gitlab.com/okotek/logboy

go 1.17

require gitlab.com/okotek/okolog v0.0.0-20220408231632-406a0d298432

require (
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	gitlab.com/okotech/loadoptions v0.0.0-20220719225702-2feb04b55a80 // indirect
	gitlab.com/okotek/okotypes v0.0.0-20220719220355-02567a19ce5f // indirect
	gitlab.com/okotek/sec v0.0.0-20220326221119-e10389566a6b // indirect
	gocv.io/x/gocv v0.29.0 // indirect
	golang.org/x/crypto v0.0.0-20220331220935-ae2d96664a29 // indirect
)

replace gitlab.com/okotek/okolog => ../okolog
